> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Lucas Costa

### Project 2 Requirements:

1. Backward-engineer (using .NET Core) the console application
2. Display short assignment requirements
Display *your* name as “author”
3. Display current date/time (must include date/time, your format preference)
4. Must perform and display room size calculations, must include data validation
and rounding to two decimal places.



#### README.md file should include the following items:

* Screenshot of the program

> This is a blockquote.
>
> This is the second paragraph in the blockquote.


##### Assignment Screenshot:

* Screenshot of program:

![program Screenshot](img/program.png)
