LIS 4369 - Extensible Enterprises Solutions

##### Assignments:

1)[A1 README.md](a1/README.md)

  * Install .NET Core
  * Create hwapp application
  * Create aspnetcoreapp application
  * Provide screenshots of installations
  * Create a Bitbucket tutorials
  * Provide git command description

2)[A2 README.md](a2/README.md)

  * Create a simple calculator with c#
  * Display current date and time
  * Validate user input
  * Create parameters for non valid divisions (by 0)

3)[A3 README.md](a3/README.md)

  * Create a Future calculator with C#
  * Display current date and time
  * Validate user input
  * Perform mathematical operations

4)[A4 README.md](a4/README.md)

  * Create a inheritance program that uses 2 different classes
  * Display current date and time
  * Use both classes to display, edit, and create constructors
  * Perform data validation

5)[A5 README.md](a5/README.md)

  * Backward-engineer (using .NET Core) the proram given
  * Display short assignment requirements.
  * Display *your* name as “author.”
  * Display current date/time (must include date/time, your format preference).
  * Create two classes: person and student (see fields and methods below).
  * Must include data validation on numeric data.

6)[Project 1: README.md](project1/README.md)

  * Create room_calculator by:
  * Backward-engineer (using .NET Core) the console application screenshot
  * Display short assignment requirement and display *your* name as “author”
  * Display current date/time (must include date/time, your format preference)
  * Must perform and display room size calculations
  * Must include data validation
  * Rounding to two decimal places.
  * Each data member must have get/set methods, also GetArea and GetVolume

7)[Project 2: README.md](project2/README.md)

  * Backward-engineer (using .NET Core) the console application
  * Creating OOP program using C#
  * Display short assignment requirements
  * Display *your* name as “author”
  * Display current date/time (must include date/time, your format preference)
  * Must perform and display room size calculations
  * Must include data validation
  * rounding to two decimal places.     
